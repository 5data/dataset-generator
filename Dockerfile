FROM bitnami/symfony

RUN apt update
RUN apt install -y wget
RUN wget https://get.symfony.com/cli/installer -O - | bash

COPY webui /app/myapp/

WORKDIR /app/myapp

CMD php /app/myapp/bin/console assets:install; $HOME/.symfony/bin/symfony serve
