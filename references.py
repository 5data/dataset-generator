campus = [
    None,
    'Caen',
    'Clermont-Ferrand',
    'Grenoble',
    'Lille',
    'Lyon',
    'Marseille',
    'Montpellier',
    'Nantes',
    'Nice',
    'Orléans',
    'Paris',
    'Reims',
    'Rennes',
    'Strasbourg',
    'Toulouse',
    'Tours',
    'Troyes',
    'Martinique',
    'Guadeloupe',
    'Réunion'
]

# Each array in this array represents the degrees of a year of study
# In Bac+1, there is no degrees, so the first array is empty
# The Bac+2 array contains the degrees of this level
# ect...
diplomes = [[], ["DUT Informatique", "DUT GE2I", "BTS SIO SLAM", "BTS SIO SSIR"], ["license"], []]

bacs = ["BAC S", "BAC ES", "BAC L", "BAC SENTR", "BAC SEN", "BAC SN"]
leaving_reasons = ["Diplome voulu obtenu", "Financières", "Personnelle", "Aller à une autre école", "Réorientation"]
lycees = ["Lycée Balzac, Tours", "Lycée Descartes, Tours"]
post_bac_schools = ["IUT Orléans", "IUT Tours", "Fac Informatique Tours"]

name = [
    'Journées portes ouverte',
    'Journée de rencontre professionnel',
    'BattleDev',
    'Game jam',
    'Le Monde Informatique IT TOURS',
    'TOSA',
    None
]