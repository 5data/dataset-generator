from datetime import datetime
from random import randint, choice
from faker import Faker

import references
from utils import random_date


def insert_student(db):
    fake = Faker('fr_FR')
    for x in range(1, 501):

        promotion = randint(1, 6)
        start_date_simu = datetime.strptime('1/1/2000', '%m/%d/%Y')
        end_date_simu = datetime.strptime('1/1/2021', '%m/%d/%Y')
        start_date_birth = datetime.strptime('1/1/1980', '%m/%d/%Y')
        end_date_birth = datetime.strptime('1/1/2000', '%m/%d/%Y')


        leaving_date = random_date(start_date_simu, end_date_simu)
        arriving_date = random_date(start_date_simu, leaving_date)
        last_pomotion_ending = leaving_date.replace(month=11, day=1)
        if last_pomotion_ending > leaving_date:
            last_pomotion_ending = last_pomotion_ending.replace(year=last_pomotion_ending.year-1)
        last_pomotion_start = last_pomotion_ending.replace(year=last_pomotion_ending.year - 1)

        diplomes = []
        if promotion > 5 and arriving_date <= last_pomotion_start:
            diplomes.append({"diplome": "MSc.2", "obtention_date": leaving_date})
            last_pomotion_ending = last_pomotion_start
            last_pomotion_start = last_pomotion_start.replace(year=last_pomotion_ending.year - 1)
        diplomes_supinfo = ["ASc.1", "ASc.2", "B.Sc", "MSc.1"]
        supinfo_campus = choice(references.campus)
        for i in [4, 3, 2, 1]:
            if promotion > i:
                if arriving_date <= last_pomotion_ending:
                    diplomes.append({"diplome": diplomes_supinfo[i-1], "obtention_date": last_pomotion_ending, "etablissement": ("Supinfo " + str(supinfo_campus) if supinfo_campus is not None else None)})
                else:
                    if i not in [1, 4]:
                        diplomes.append({"diplome": choice(references.diplomes[i-1]), "obtention_date": last_pomotion_ending, "etablissement": choice(references.post_bac_schools)})
                last_pomotion_ending = last_pomotion_start
                last_pomotion_start = last_pomotion_start.replace(year=last_pomotion_ending.year - 1)

        diplomes.append({"diplome": choice(references.bacs), "obtention_date": last_pomotion_ending, "etablissement": choice(references.lycees)})
        leaving_reason = None
        if leaving_date <= datetime.strptime('30/10/2019', '%d/%m/%Y'):
            if promotion == 6:
                leaving_reason = "Master obtenu"
            else:
                leaving_reason = choice(references.leaving_reasons)

        student = {
            'full_name': fake.name(),
            'birth': random_date(start_date_birth, end_date_birth),
            'promotion': promotion,
            'diplomes': diplomes,
            'campus': supinfo_campus,
            'arriving_date': arriving_date,
            'leaving_date': None if leaving_date > datetime.strptime('30/10/2019', '%d/%m/%Y') else leaving_date,
            'leaving_reason': leaving_reason
        }

        result = db.student.insert_one(student)
