<?php

namespace App\Controller;

use App\Entity\Campus;
use App\Entity\Etudiant;
use App\Entity\Flat;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Name;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $repoE = $em->getRepository(Etudiant::class);
        $repoF = $em->getRepository(Flat::class);
        $repoC = $em->getRepository(Campus::class);

        $E = $repoE->findAll();
        $F = $repoF->findAll();
        $C = $repoC->findAll();
        $E1 = array();
        //$F1 = array();
        $C1 = array();
        $E2 = array();
        //$F2 = array();
        $C2 = array();
        for ($i=0;$i< count($E);$i++) {
            array_push($E1, $E[$i]->getEtudiant());
            array_push($E2, $E[$i]->getAnnee());

        }
        /*
        for ($x=0;$x< count($F);$x++) {
            array_push($F1, $F[$x]->getName());
            array_push($F2, $F[$x]->getPercentFlat());

        }
        */
        for ($q=0;$q< count($C);$q++) {
            array_push($C1, $C[$q]->getName());
            array_push($C2, $C[$q]->getPercent());
        }
        //dd($E1, $E2, $C1, $C2);

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'etudiant' => $E1,
            'etudiant2' => $E2,
            'flat' => $F,
            'campus' => $C1,
            'campus2' => $C2,
        ]);
    }
}
