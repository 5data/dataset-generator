from pymongo import MongoClient
from lib import Spark
# pprint library is used to make the output look more pretty
from pprint import pprint

from insert_events import insert_events
from insert_student import insert_student

if __name__ == '__main__':
    # connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string
    client = MongoClient("mongodb://root:example@c-est.party")
    db = client.cinq_data

    # drop collections to start with empty ones
    db.student.drop()
    db.events.drop()

    # insert dummy values
    insert_student(db)
    insert_events(db)

    spark = Spark.SparkConnection()
