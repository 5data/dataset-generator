import time
import random
from random import choice
from references import name, campus

def insert_events(db):
    i = 500
    for x in range(i):
        if (x % 11) != 4:
            t1 = time.mktime(time.strptime('1/1/2000', '%m/%d/%Y'))
            t2 = time.mktime(time.strptime('11/28/2020', '%m/%d/%Y'))
            tf = t1 + random.random() * (t2 - t1)
            event = {
                'NAME': choice(name),
                'DATE': time.strftime('%m/%d/%Y', time.localtime(tf)),
                'CAMPUS': choice(campus)
            }

        else:
            event = {
                'NAME': choice(name),
                'DATE': None,
                'CAMPUS': choice(campus)
            }
        result = db.events.insert_one(event)
        print(f'Created {x} of {i} as {result.inserted_id}')
