#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pyspark.sql import SparkSession
import sys

sys.stdout = open(sys.stdout.fileno(), mode='w', encoding='utf8', buffering=1)


def SparkConnection():

    my_spark = SparkSession \
        .builder \
        .appName("5Data") \
        .config("spark.mongodb.input.uri", "mongodb://root:example@c-est.party/") \
        .config("spark.mongodb.input.database", "cinq_data") \
        .config("spark.mongodb.input.collection", "student") \
        .config("spark.jars.packages", "org.mongodb.spark:mongo-spark-connector_2.11:2.2.0") \
        .getOrCreate()

    df = my_spark \
        .read \
        .format("com.mongodb.spark.sql.DefaultSource") \
        .option("user", "root") \
        .option("password", "example") \
        .load()

    return df






# df.printSchema()

# leaving_without_diploma = df.select("*").filter(df.leaving_reason != "Diplome voulu obtenu").count()
# all = df.select("*").count()

# print(f'{(leaving_without_diploma/all) * 100}% ({leaving_without_diploma}/{all}) of students leave without diploma ')

# students = df.select("full_name").collect()
#
# for student in students:
#     print(student[0].encode('utf-8', 'backslashreplace').decode('utf-8'))
